#############
# day1
#############
## variable and assignment

#variable
#valid
#1. variable starting char
#2. variable starting underscore
#3. variable starting char followed up by no.

##############
#valid eg
# a=23
# print(a)
#  _a=34
# print(_a)
# a1=455
# print("a1")

################
#invalid
# @a1=23
# print(@a)
# $a=44
# print($a)

###############
#note: prefer camelcase for creating a variable name

##########
##Assignment
##########

#Python supports multible assignment
#a,b =23,45
#print(a,b)
# for swapping of two 
# a=46
# b=56
# a,b=b,a
# print(a,b)
############
##data type
## Number system
# a=23
# b=23.32
# c=23+45j
# print(a,b,c)
# print(type(a),type(b),type(c))
##############
## String
# a='hello world'
# b="good evening"
# c='''enjoy your tea'''
# d="""Be ready for RBG system"""
# print(a,b,c,d,type(a))

##List
# list1=[1,2,3,4,5]
# list2=[2,"hello",[2,3,4]]
# print(list1)
# print(list2)
# print(type(list1))

##Tuple
# tup1=(1,2,3,4,5)
# tup2=(2,"hello",(2,4,3))
# print(tup1)
# print(tup2)
# print(type(tup2))

##Dictionary
# dict1={"name":"lokesh",
#        "std":"BE",
#         "roll_no":26,
#         "courses": ["java","python"]
#         }
# print(dict1)
# print(type(dict1))

##Set
# set1={1,2,3,4,5}
# set2={2,3,4,5,6}
# print(set1.issubset(set2))
# print(set1)
# print(set2)
# print(type(set2))

##Operators
#Arithmetic
# a=10
# b=5
# print(f"add:-{a+b}, sub :- {a-b}")
# print(f"multiple:-{a*b}")      
# f""--> means it helps for formating-- string and {} it uses 
# for expression.
# i want to format string and data value.

# a=3
# b=2
# print(a/b)  ans-> 1.5 exact divide

# a=3
# b=2
# print(a//b) ans-> 1 quotient value

# a=-3
# b=2
# print(a/b)
# print(a//b) 
#if any of the operand id -ve then the ans will be floor value mns lower
#value

# a=10
# b=3
# print(a**b)

# a=23
# b=10
# a+=b
# print(a)
############
#Comparison
# a=10
# b=12
# c=14
# print(a>b)
# print(a<=b)
# print(a>=b)
# print(a==c)
# print(a>b<=c)
# print(a<b>c)

##logical
# a=0
# b=23
# c=45
# print(a and b and c)
# print(a or b or c)
# print(a or b and b or c)
# print(a-b or b-a or a-c or c-a)

##Bitwise
# a=5
# b=10
# print(a&b)
# print(a|b)
# print(b>>1)

##Membership
# list1=[1,2,3,4,5]
# a="hello world"
# print(2 in list1)
# print('w' in a)
# print('world' not in a )

##Identity
# a=23
# b="23"
# print(type(a) is type(b))
# print(type(a) is not type(int(b)))

############################
##Input function
# a=input("enter no")
# print(a)
# print(type(a))

####code to find ROOT####
#print(float(input("enter no"))**(1/float(input("enter root"))))

####################
##Decision making
##If statement 
#a=23
#b=34
# if a>b: 
# 	print("a is greater!!")
# else: 
#      print("b is greater")	

#print("a is greater") if(a>b) else print("b is greater")

## if elif else statement
# a=10
# b=15
# c=20
# if a>b and a>c: 
# 	print("a is greater")
# elif b>a and b>c:
# 	print("b is greater")
# else:print("c is greater")

#################
# print("a is greater") if a>b and a>c else print("b is greater") if b>a and b>c else print("c is greater")

#################
# print(range(1,10))

# print(list(range(1,10)))

# print(list(range(20)))

# print(list(range(0,100,2)))
# print(list(range(1,100,3)))

# print(list(range(100,1,-1)))

# print(list(range(99,0,-2)))

######################
##Loop
# for i in "hello world":
	# print(i)

#for i in "hello  world":
	#print(i,end=" ")

# for row in list(range(1,101)):
# 	for col in list(range(1,11)):
# 		print(row*col, end=" ")
# print()	

# ##while
# counter=0
# while counter<=10:
# 	print("hello all")
# 	counter+=1 
# else:
# 	print("good bye")

##break and continue
# for i in "hello world":
# 	if i=="w":
# 		break
# 	print(i)

# for i in "hello world":
# 	if i=="w":
# 		continue
# 	print(i)

############################
a=input(enter the price of starter)
b=input(enter the price of main course)
c=input(enter the price of desert)
d=(a+b+c)
e=d*0.15
















































































